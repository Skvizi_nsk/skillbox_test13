#pragma once
#include <cmath>

int KvadSum(int a, int b)
{
	return pow(a, 2) + pow(b, 2);
}